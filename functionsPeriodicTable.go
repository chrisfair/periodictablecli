package main

import (
	"errors"
)

// IsValidSymbol returns true if the symbol is valid
func (p *PeriodicTable) IsValidSymbol(symbol string) bool {
	for i := 0; i < len(p.Elements); i++ {
		if p.Elements[i].Symbol == symbol || p.Elements[i].Name == symbol {
			return true
		}
	}
	return false
}

// IsValidName returns true if the name is valid
func (p *PeriodicTable) IsValidName(name string) bool {
	for i := 0; i < len(p.Elements); i++ {
		if p.Elements[i].Name == name {
			return true
		}
	}
	return false
}

// GetElectronConfigurationFromSymbol returns the electron configuration of an element from its symbol or an error when the symbol is not found
func (p *PeriodicTable) GetElectronConfigurationFromSymbol(symbol string) (string, error) {
	for i := 0; i < len(p.Elements); i++ {
		if p.Elements[i].Symbol == symbol || p.Elements[i].Name == symbol {
			return p.Elements[i].ElectronConfiguration, nil
		}
	}
	return "", errors.New("Element not found")
}

// GetAtomicWeightFromAtomicNumber returns the atomic weight of an element from its atomic number or an error when the atomic number is not found
func (p *PeriodicTable) GetAtomicWeightFromAtomicNumber(atomicNumber int) (float64, error) {
	for i := 0; i < len(p.Elements); i++ {
		if p.Elements[i].AtomicNumber == atomicNumber {
			return p.Elements[i].AtomicWeight, nil
		}
	}
	return 0, errors.New("Element not found")
}

// GetElectroNegativityFromSymbol returns the electronegativity of an element from its symbol or an error when the symbol is not found
func (p *PeriodicTable) GetElectroNegativityFromSymbol(symbol string) (float64, error) {
	for i := 0; i < len(p.Elements); i++ {
		if p.Elements[i].Symbol == symbol || p.Elements[i].Name == symbol {
			return p.Elements[i].ElectroNegativity, nil
		}
	}
	return 0, errors.New("Element not found")
}

// GetAtomicNumber returns the atomic number of an element from its symbol or an error when the symbol is not found
func (p *PeriodicTable) GetAtomicNumberFromSymbol(symbol string) (int, error) {
	for i := 0; i < len(p.Elements); i++ {
		if p.Elements[i].Symbol == symbol || p.Elements[i].Name == symbol {
			return p.Elements[i].AtomicNumber, nil
		}
	}
	return 0, errors.New("Element not found")
}

// GetAtomicWeight returns the atomic weight of an element from its symbol or an error when the symbol is not found
func (p *PeriodicTable) GetAtomicWeightFromSymbol(symbol string) (float64, error) {
	for i := 0; i < len(p.Elements); i++ {
		if p.Elements[i].Symbol == symbol || p.Elements[i].Name == symbol {
			return p.Elements[i].AtomicWeight, nil
		}
	}
	return 0, errors.New("Element not found")
}

// GetElementName returns the name of an element from its symbol or an error when the symbol is not found
func (p *PeriodicTable) GetElementNameFromSymbol(symbol string) (string, error) {
	for i := 0; i < len(p.Elements); i++ {
		if p.Elements[i].Symbol == symbol || p.Elements[i].Name == symbol {
			return p.Elements[i].Name, nil
		}
	}
	return "", errors.New("Element not found")
}

// GetElementInfo returns the information of an element from its symbol or an error when the symbol is not found
func (p *PeriodicTable) GetElementInfoFromSymbol(symbol string) (Element, error) {
	for i := 0; i < len(p.Elements); i++ {
		if p.Elements[i].Symbol == symbol || p.Elements[i].Name == symbol {
			return p.Elements[i], nil
		}
	}
	return Element{}, errors.New("Element not found")
}

// GetSymbolFromGroupAndPeriod returns the symbol of an element from its group and period
func (p *PeriodicTable) GetSymbolFromGroupAndPeriod(group int, period int) (string, error) {
	for i := 0; i < len(p.Elements); i++ {
		if p.Elements[i].Group == group && p.Elements[i].Period == period {
			return p.Elements[i].Symbol, nil
		}
	}
	return "", errors.New("Element not found")
}
